
module Vector where
import Data.Vector.Unboxed		(Vector)
import qualified Data.Vector.Unboxed	as V


mergesort :: Vector Int -> Vector Int
mergesort  vec
		| vlen == 0 = V.empty
		| vlen == 1 = vec
		| otherwise = merge (mergesort  xs1) (mergesort  xs2)
			where 
				vlen = V.length vec 
				xs1 = V.take (vlen `quot` 2) vec
				xs2 = V.drop (vlen `quot` 2) vec

merge :: Vector Int -> Vector Int -> Vector Int
merge a b
	| V.length b == 0 = a
	| V.length a == 0 = b
	| otherwise 	  = if (V.head a) <= (V.head b) then V.cons (V.head a) $ merge (V.tail a) b
												else V.cons (V.head b) $ merge a (V.tail b)