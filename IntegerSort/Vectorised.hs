{-# LANGUAGE ParallelArrays #-}
{-# OPTIONS -fvectorise #-}
module Vectorised (mergesortPA) where
import Data.Array.Parallel
import Data.Array.Parallel.Prelude.Int  as I
import qualified Prelude
import Debug.Trace

mergesortPA :: PArray Int -> PArray Int
mergesortPA xs = toPArrayP (mergesort (fromPArrayP xs))

mergesort :: [:Int:] -> [:Int:]
mergesort  vec
		| vlen I.== 0 = emptyP
		| vlen I.== 1 = singletonP (indexP vec 0)
		| otherwise = merge (mergesort  xs1) (mergesort  xs2)
			where 
				vlen = lengthP vec 
				splitMark = I.div vlen 2
				xs1 = sliceP 0 splitMark vec
				xs2 = sliceP splitMark (vlen I.- splitMark) vec

merge :: [:Int:] -> [:Int:] -> [:Int:]
merge a b
	| blen I.== 0 = a
	| alen I.== 0 = b
	| otherwise 	  = if (indexP a 0) I.<= (indexP b 0) 
		then appendP (head a) (merge (tail a) b)
		else appendP (head b) (merge a (tail b))
	where 
		alen = lengthP a
		blen =  lengthP b

head :: [:Int:] -> [:Int:]
head v = singletonP (indexP v 0)

tail :: [:Int:] -> [:Int:]
tail v = sliceP 1 (lengthP v I.- 1) v