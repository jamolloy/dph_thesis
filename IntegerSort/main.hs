import Vectorised
import Timing
import System.Environment
import Data.Vector.Unboxed		(Vector)
import Data.Array.Parallel	as P
import Data.Array.Parallel.PArray	as P
import qualified Data.Vector.Unboxed	as V
import qualified Vector 		as V
import Data.Maybe
import Data.List
import System.Random

main :: IO ()
main 
 = do	
 	args	<- getArgs
	case args of
	  [alg, num] -> do
 		--f <- readFile "input.data"
	  	--let ints = parseIntInput $ lines f	
   		rand  <- newStdGen
   		let numInt = read num
   		let ints = randomlist numInt (-numInt, numInt) rand
	  	run alg ints
	  _		-> usage

usage	
 = putStr $ unlines
	[ "Usage: IntegerSort alg sampleSize"
	, "       alg one of " ++ show ["vectorised", "vector"] ]

-- | Run the benchmark
run :: String -> [Int] -> IO ()

run "vectorised" xs
 = do	(sorted, tElapsed)
	 <- time $ let 	sorted	= mergesortPA (P.fromList xs)
		   in	sorted `seq` return sorted
				
	-- Print how long it took.
	putStr $ prettyTime tElapsed

	-- Print a checksum of all the primes.
	--putStrLn $ show $ P.toList sorted

run "vector" xs
 = do	(sorted, tElapsed)
	 <- time $ let 	sorted	= V.mergesort (V.fromList xs)
		   in	sorted `seq` return sorted

	-- Print how long it took.
	putStr $ prettyTime tElapsed

	-- Print a checksum of all the primes.
	--putStrLn $ show $ sorted
	

run _ _	= usage

parseIntInput :: [String] -> [Int]
-- check sequence is of type int
--let valueType = fmap (compare "Int") (stripPrefix "sequence" x)
parseIntInput (x:xs) = map read xs
 
randomlist :: Int -> (Int, Int) -> StdGen -> [Int]
randomlist n range gen = take n $ randomRs range gen