import Data.List

main = do
	f <- readFile "input.data"
	let unsorted = parseIntInput $ lines f
	putStr $ concatMap show unsorted
	putStr $ concatMap show $ mergeSort unsorted

mergeSort :: (Ord a) => [a] -> [a]
mergeSort  [] = []
mergeSort  [x] = [x]
mergeSort  xs = merge  (mergeSort  xs1) (mergeSort  xs2)
	where
		(xs1, xs2) = splitAt (length xs `quot` 2) xs

merge :: (Ord a) => [a] -> [a] -> [a]
merge [] ys = ys
merge xs [] = xs
merge (x:xs) (y:ys) = if x <= y
					then x : merge xs (y:ys)
					else y : merge (x:xs) ys


-- read sequence<type>
-- v
-- v2... etc

parseIntInput :: [String] -> [Int]
-- check sequence is of type int
--let valueType = fmap (compare "Int") (stripPrefix "sequence" x)
parseIntInput (x:xs) = map read xs
