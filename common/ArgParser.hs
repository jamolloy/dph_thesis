module ArgParser (PBBSargs(PBBSargs), compilerOpts, processInput, getInt, getPair, writeOutputToFile) where

import System.Console.GetOpt
import qualified Data.ByteString.Lazy.Char8 as B
import qualified Data.Vector.Unboxed as U
import Control.Monad (liftM)
import System.IO

import Data.Maybe (fromMaybe)

data PBBSargs = PBBSargs {
	rounds :: Int,
	output :: Maybe String,
	input :: String
}

data OptFlag = Rounds Int | Output String | Input String
	deriving Show

options :: [OptDescr OptFlag]
options = [
    Option ['r'] ["rounds"] 	(ReqArg (Rounds . read) "Int" )     "Number of test rounds",
    Option ['o'] ["output"]   	(ReqArg Output "FILE")     						"Optional output file"
  ]

compilerOpts :: [String] -> IO PBBSargs
compilerOpts argv = 
   case getOpt RequireOrder options argv of
      (
        (Rounds n : Output o : _) ,[i],[] ) -> return $ PBBSargs n (Just o) i
      (
        [],[i],[] ) -> return $ PBBSargs 1 Nothing i
      (
      	(Rounds n : _) ,[i],[] ) -> return $ PBBSargs n Nothing i
      (_,_,errs) -> ioError (userError (concat errs ++ usageInfo header options))
  where header = "Usage: ic [OPTION...] files..."

writeOutputToFile output result = case output of
        Just outputFile -> do
            toHandle <- openFile outputFile WriteMode
            hPutStrLn toHandle "sequenceInt"
            U.mapM_ (hPrint toHandle) result
            hClose  toHandle
        _ -> return ()

processInput action = (liftM f) . B.readFile
    where
        f = U.unfoldr (action . B.readInt) . bsNextline

getPair x = case x of
    Nothing -> Nothing
    Just (n,bs2) -> case B.readInt (B.tail bs2) of
        Nothing -> Nothing
        Just (n2,bs3) ->    if not (B.null bs2) then Just ((n,n2),B.tail bs3) 
                            else Just ((n,n2),B.empty)

getInt x = case x of
    Nothing -> Nothing
    Just (n,bs2) -> if not (B.null bs2) then Just (n,B.tail bs2) 
                    else Just (n,B.empty)

bsNextline = B.tail . (B.dropWhile (/= '\n'))

