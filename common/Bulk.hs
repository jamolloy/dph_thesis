{-#  OPTIONS_GHC -XBangPatterns #-}

module Bulk where

import Data.Vector.Unboxed   (Vector, Unbox, MVector, indexM)
import qualified Data.Vector.Unboxed  as U (length, slice, mapM_, enumFromN, last)
import qualified Data.Vector.Unboxed.Mutable  as MU
import qualified Data.Vector  as V 
import qualified Data.Vector.Mutable  as MV (slice, copy, replicate, set)
import qualified Data.Vector.Generic.Mutable as M
import GHC.ST 
import Gang
import Debug.Trace
import Control.Monad
import DR


type Steps a s = V.Vector (a s)
type Keep s = MU.MVector s Bool 
type Operation step s = step s -> Int -> ST s Bool

data OpType = ReserveT | CommitT deriving (Show)

newKeep :: Int -> ST s (Keep s)
newKeep n = MU.replicate n True

bulk :: DRStep step input mut output => OpType -> Operation step s -> Gang -> Vector Int -> Steps step s -> Keep s -> ST s ()
bulk opType operation gang prefix steps keep = do
  gangST gang "bulk" WorkUnknown (bulk' opType operation prefix steps keep (gangSize gang)) 

-- assume length of steps will be same as gang size. 
--TODO: move (freserve operation prefix steps keep) out to bulk
bulk' :: DRStep step input mut output => OpType -> Operation step s -> Vector Int -> Steps step s -> Keep s -> Int -> Int -> ST s ()
bulk' opType operation prefix steps keep gs threadId = case opType of 
    ReserveT ->  {-# SCC "thread_reserve" #-} V.mapM_ (freserve operation prefix steps keep) vs --TODO: mapM_ is where the alloc/time problem is????
    CommitT ->  {-# SCC "thread_commit" #-} V.mapM_ (fcommit operation prefix steps keep) vs
  where 
    (!ind, !chunkSize) = calculateSlice gs threadId (U.length prefix)
    vs = V.enumFromN ind chunkSize --These numbers are always the same. we dont want to be
  


freserve, fcommit :: DRStep step input mut output => Operation step s -> Vector Int -> Steps step s -> Keep s -> Int -> ST s ()
freserve operation prefix steps keep i = do
    index <- indexM prefix i
    s <- V.indexM steps i
    v <- operation s index
    MU.write keep i $! v    --TODO: can we just zip prefix, steps, keep together and then map?
fcommit operation prefix steps keep i = do 
    b <- MU.read keep i
    when b $ do
      index <- indexM prefix i 
      v <- V.indexM steps i 
      b1 <- operation v index
      MU.write keep i $! (not b1) --need strictness here due to some weird thunk segfault


bulkReserve, bulkCommit :: DRStep step input mut output => input -> mut s -> Gang -> Vector Int -> Steps step s -> Keep s -> ST s ()
bulkReserve input mutStuff theGang prefix prefixSteps keep = bulk ReserveT (reserve input mutStuff) theGang prefix prefixSteps keep
bulkCommit input mutStuff theGang prefix prefixSteps keep = bulk CommitT (commit input mutStuff) theGang prefix prefixSteps keep


calculateSlice :: Int -> Int -> Int -> (Int, Int)
calculateSlice gs threadId vecLength = (ind, chunkSize)
  where
    l = vecLength
    maxChunkSize = (l + gs - 1) `div` gs
    chunkSize = if (threadId < gs - 1) 
                then min (l-ind) maxChunkSize
                else max (l - (gs - 1)* maxChunkSize) 0
    ind = min (threadId * maxChunkSize) (l-1)  
