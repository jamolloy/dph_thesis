module Main where

import Gang
import Bulk
import Timing
import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Generic.Mutable as M
import GHC.ST
import System.IO.Unsafe 
import Control.Concurrent
import Control.Monad.ST.Unsafe
-- import Control.DeepSeq

theGang :: Gang
theGang = unsafePerformIO (getNumCapabilities >>= forkGang)
{-# NOINLINE theGang #-}



mainST size  = do 
  { mvec <- M.replicate size 0
  ; let ivec = U.generate size (\i -> (2*i, i))
  -- enforce evaluation of mvec, ivec
  ; x <- M.read mvec 0
  ; x `seq` (ivec U.! 0) `seq` return ()
  ; (_, t) <- timeST $ bulkCommit2 theGang mvec ivec
  ; mv <- U.unsafeFreeze mvec
  ; return (mv, t)
  }

main :: IO ()
main  = do 
  { let (v, t) = runST $ mainST 1000000
  ; putStr	$ prettyTime t
  ; putStrLn $ show $ (U.toList v) !!50
  }