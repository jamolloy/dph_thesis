{-#  OPTIONS_GHC -XMultiParamTypeClasses -XFunctionalDependencies #-}
module DR (DRStep(..), ReserveVec, createR,reserveR,reservedR,checkR,resetR, filterR, Algo) where

import GHC.ST
import Debug.Trace
import qualified Data.Vector.Unboxed.Mutable as MU
import qualified Data.Vector.Unboxed as U


class DRStep step input mut output 		| mut -> step, step -> mut
										, step -> input
										, step  -> output where 
--two way functionaldependencies seem like a bit of a hack???? 
--should we just pair the types somehow?
	newStep :: ST s (step s)
	reserve :: input -> mut s -> step s -> Int -> ST s Bool --e.g. instance DRStep Edge STStep for spanning tree, or instance DRStep AdjVertex MISStep.
	commit  :: input -> mut s -> step s -> Int -> ST s Bool

	buildSharedStructures :: input -> ST s (mut s)
	returnResult :: mut s -> ST s output


type Vindex = Int

type Algo step = forall s. ST s (step s)

newtype ReserveVec s = Reserve (MU.MVector s Int)

createR :: Int -> ST s (ReserveVec s)
createR n = do mvec <- MU.replicate n maxBound ; return $ Reserve mvec

reserveR :: (ReserveVec s) -> Vindex -> Int -> ST s ()
reserveR (Reserve mvec) index val = do --TODO: not atomic!!!!!!!!!!!!
	oldval <- MU.read mvec index; 
	MU.write mvec index (min oldval val)

reservedR :: (ReserveVec s) -> Vindex -> ST s Bool
reservedR (Reserve mvec) index = do r' <- MU.read mvec index; return (r' < maxBound)

resetR :: (ReserveVec s) -> ST s () 
resetR (Reserve mvec) = MU.set mvec maxBound

checkR :: (ReserveVec s) -> Vindex -> Int -> ST s Bool
checkR (Reserve mvec) index val = do r' <- MU.read mvec index; return (r' == val)

filterR :: (ReserveVec s) -> ST s (U.Vector Int)
filterR (Reserve res) = do
	frozenRes <- U.unsafeFreeze res
	return $ U.filter (\x -> x < maxBound) frozenRes


