{-#  OPTIONS_GHC -XFlexibleInstances #-}
{-#  OPTIONS_GHC -XMultiParamTypeClasses #-}
{-#  OPTIONS_GHC -XGeneralizedNewtypeDeriving #-}
{-#  OPTIONS_GHC -XBangPatterns #-}

module Main (main) where

import qualified Data.Vector.Unboxed as U
import qualified Data.Vector.Generic.Base as GBV (Vector)
import qualified Data.Vector.Generic.Mutable as GMV (MVector)
import qualified Data.Vector.Unboxed.Mutable as MV
import qualified Data.Vector as V
import DR
import Data.STRef
import GHC.ST
import Control.Monad (when, replicateM_, liftM)
import Control.Monad.ST (stToIO)
import System.IO
import Debug.Trace
import Control.DeepSeq
import System.Mem

import SpeculativeFor
import Timing
import ArgParser
import System.Environment (getArgs)


newtype Flag = Flag Int deriving (Eq,Ord,Enum, GBV.Vector U.Vector, GMV.MVector U.MVector, U.Unbox)
(flive:fin:fout:_) = [Flag 1 ..]
{- see http://stackoverflow.com/questions/10866676/how-do-i-write-a-data-vector-unboxed-instance-in-haskell
and http://www.haskell.org/haskellwiki/Performance/Data_types
-}

instance Show Flag where
	show flag 	| flag == flive = "0"
				| flag == fin   = "1"
				| flag == fout  = "2"
				| otherwise 	 = "???"

type Degree = Int
type ST' s a = ST s (a s)

type AdjArray = V.Vector Vertex
type FlagArray s = MV.MVector s Flag
type Neighbours = U.Vector Int

type Vertex = Neighbours

type MISInput = AdjArray
type MISOutput = U.Vector Flag

newtype MISMut s = MISMut (FlagArray s)
newtype MISStep s = MISStep (STRef s Flag)

instance DRStep MISStep MISInput MISMut MISOutput where --TODO: make all this mutable
	
	newStep = do
		fl <- newSTRef fin
		return $! MISStep fl

	reserve vertices (MISMut flags) (MISStep fl) index = do
		let v = vertices V.! index
		writeSTRef fl fin
		--TODO: stuff goes here, (Checking neighbours)
		res <- checkNeighbours index flags fl v
		return $! res
		
	commit vertices (MISMut flags) (MISStep fl) index = do
		flag <- readSTRef fl
		MV.write flags index flag
		return $! (flag > flive)

	buildSharedStructures graph = do
		flags <- MV.replicate (V.length graph) flive
		return $! MISMut flags

	returnResult (MISMut flags) = U.unsafeFreeze flags




checkNeighbours :: Int -> FlagArray s -> STRef s Flag -> Neighbours -> ST s Bool
checkNeighbours index flags fl neighbourArray = do
	flv <- readSTRef fl
	if flv == fout then return True
	else if U.null neighbourArray then return True
	else do
		ngh <- U.headM neighbourArray
		when (ngh < index) $ do
			ngh_flag <- MV.read flags ngh 
			if ngh_flag == fin then writeSTRef fl fout
			else when(ngh_flag == flive) $ writeSTRef fl flive

		checkNeighbours index flags fl (U.tail neighbourArray)


readGraphArray :: U.Vector Int -> (AdjArray, Int, Int)
readGraphArray vec = (graph, n, m)
	where
		n = vec U.! 0
		m = vec U.! 1
		offsets = U.slice 2 n vec
		!edges = U.force $ U.slice (n+2) m vec
		!graph = force $ V.map (\i -> getNeighbours offsets edges i) $ V.enumFromN 0 n
		
		getNeighbours offsets edges i = U.slice start len edges 	
			where
				start = offsets U.! i
				len = (if i == U.length offsets - 1 then m else offsets U.! (i+1)) - start
		--TODO: out of bounds check (using min)

graphFromInput :: String -> IO (AdjArray, Int, Int)
graphFromInput input = do
	res <- processInput getInt input
	return $! readGraphArray res
	
testMIS :: MISInput -> IO MISOutput
testMIS graph = do
	(result, t) <- time $ return $! speculative_for 100 (newStep :: Algo MISStep) graph
	putStrLn $ pbbsTime t
	return result

main = do
	args <- getArgs
	(PBBSargs rounds output input) <- compilerOpts args

	(graph, n, m) <- graphFromInput input
	--do n rounds. write time to std out. (pbbs-time)
	replicateM_ (rounds - 1) $ do
		testMIS graph
	result <- testMIS graph

	writeOutputToFile output result




