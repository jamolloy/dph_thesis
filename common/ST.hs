{-#  OPTIONS_GHC -XMultiParamTypeClasses #-}
{-#  OPTIONS_GHC -XFlexibleInstances #-}

module ST (newStep, DRStep(reserve, commit), STStep, Edge, STmut(STmut)) where

import Data.Vector.Unboxed  as U (Unbox, Vector, indexM, (!), maximum, map)

import UnionFind
import Data.STRef
import GHC.ST
import Debug.Trace
import DR

type Index = Int

type Vertex = Int
type Edge   = (Vertex, Vertex)

data STmut s = STmut (ReserveVec s) (UF s) 

type STinput = U.Vector Edge
type SToutput = U.Vector Int 

--inverting data structure into vector of pairs. really orthoganal problem. would need extra work for dph
data STStep s = STStep (STRef s Vertex) (STRef s Vertex)


getEdge (STStep _ _ ) = "Edge ?" 

instance Show (STStep s) where
	show step = show (getEdge step)

		
instance DRStep STStep STinput STmut SToutput where --TODO: make all this mutable
	newStep = do
		u <- newSTRef 0
		v <- newSTRef 0
		return $! STStep u v
	
	reserve edges (STmut res uf) (STStep u v) index = do
			(e1, e2) <- indexM edges index
			upar <- find uf e1
			vpar <- find uf e2

			if upar == vpar then return False
			else if upar < vpar then stReserve upar vpar
			else stReserve vpar upar
		where stReserve a b = do
				writeSTRef u a
				writeSTRef v b
				reserveR res b index
				return True


	commit edges (STmut res uf) (STStep u v) index = do
		v' <- readSTRef v
		check <- checkR res v' index
		if check
		then do
			u' <- readSTRef u
			link uf v' $! u'
			return True
			--link uf v' $ traceShow (edges ! index) u'
		else return False

	buildSharedStructures input = do
		let !inputSize = (U.maximum $ U.map (uncurry max) input) + 1
		uf <- makeParents inputSize
		res <- createR inputSize
		return $! STmut res uf

	returnResult (STmut res uf) = filterR res




