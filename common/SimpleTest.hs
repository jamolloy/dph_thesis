{-#  OPTIONS_GHC -XMultiParamTypeClasses #-}
{-#  OPTIONS_GHC -XFlexibleInstances #-}
{-#  OPTIONS_GHC -XBangPatterns #-}

module Main (newStep, DRStep(reserve, commit), SimpleStep, main) where

import Data.Vector.Unboxed as V  (Unbox, Vector, indexM, (!), sum, unsafeFreeze, enumFromN)
import qualified Data.Vector.Unboxed.Mutable as MV  (MVector, write, replicate)

import GHC.ST
import Data.STRef
import Debug.Trace
import DR
import SpeculativeFor
import Timing

data SimpleStep s = SimpleStep !(Vector Int) !(MV.MVector s Int) 

newStep :: (Vector Int) -> (MV.MVector s Int) -> ST s (SimpleStep s)
newStep v mvec = do
	return $! SimpleStep v mvec

instance Show (SimpleStep s) where
	show (SimpleStep _ _) = show "step"

instance DRStep SimpleStep s where --TODO: make all this mutable
	reserve st@(SimpleStep v mvec) index = return True

	commit (SimpleStep v mvec) index = return True


main = do
	let inputSize = 2000000
	let !xs = enumFromN 0 inputSize
	let (result, t) = runST $ testST inputSize xs
	putStrLn $ "Sum: " ++ (show result)
	putStr	$ prettyTime t

testST :: Int -> Vector Int -> ST s (Int, Time)
testST inputSize xs = do
	!mvec <- MV.replicate inputSize 0
	let indicesToSteps = newStep xs mvec

	(_, t) <- timeST $ speculative_for inputSize indicesToSteps
	mvec <- unsafeFreeze mvec
	let nc =  mvec V.! 50
	return (nc, t)


