
{-#  OPTIONS_GHC -XBangPatterns #-}
{-#  OPTIONS_GHC -XScopedTypeVariables #-}
{-#  OPTIONS_GHC -XTypeFamilies #-}


module SpeculativeFor(speculative_for) where


import qualified Data.Vector  as V 
import qualified Data.Vector.Unboxed  as U
import qualified Data.Vector.Unboxed.Mutable  as MU
import qualified Data.Vector.Mutable  as MV (slice, copy, replicate, set)
import qualified Data.Vector.Generic  as G (Vector, length)
import Debug.Trace
import GHC.ST 
import System.IO.Unsafe 
import Control.Concurrent
import Control.Monad


import Bulk
import Gang
import DR

import Foreign.Marshal.Utils (fromBool)

{-
current notes:
pack prefix uses approx 13.7% runtime and 6.1% allocations - overly heavy, allocation of new vectors here

speculative_for' is 37.1% and 47.4% respec. (including pack prefix). Way too heavy!!!  
    most importantly, the createStep lambda is %12.7 and 34.8% respectively with n calls

finally the f function which runs the reserve/Commit is 24.3% and 26.1% individual. We can cut down here.

So ultimately 50% of time and 71% of alloc can be cut. This would make a massive different. 
Particularly the alloc stuff, which is not done in parallel.


-}

{-
    With large initial heap size(-A500m) the speedup is exactly in line with amdahls law (when profiled using threadscope)

-}

theGang :: Gang
theGang = unsafePerformIO (getNumCapabilities >>= forkGang)
{-# NOINLINE theGang #-}

speculative_for :: (G.Vector v a, DRStep step input mut output, v a ~ input) => Int -> Algo step -> input -> output
speculative_for granularity step input = runST $ do
        let n = G.length input
        let roundSize = (n `div` granularity) + 1
        sharedState <- buildSharedStructures input 
        --TODO: combining (prefix,rest) with prefix selection helps. 
        keep <- newKeep roundSize

        let prefix = U.enumFromN 0 roundSize

        !prefixSteps <- {-# SCC "replicateM" #-} V.replicateM roundSize step
        speculative_for' roundSize input sharedState keep prefix prefixSteps roundSize n
        returnResult sharedState
    
speculative_for' :: DRStep step input mut output => Int -> input -> mut s -> Keep s -> U.Vector Int -> Steps step s -> Int -> Int -> ST s ()
speculative_for' roundSize input sharedState keep prefix prefixSteps offset n = unless (U.null prefix) $ do
    MU.set keep True
    bulkReserve input sharedState theGang prefix prefixSteps keep
    bulkCommit input sharedState theGang prefix prefixSteps keep

    (numCommitted, newprefix, newOffset) <- packPrefix roundSize keep prefix offset n
    speculative_for' roundSize input sharedState keep newprefix prefixSteps newOffset n


packPrefix :: Int -> Keep s -> U.Vector Int -> Int -> Int -> ST s (Int, U.Vector Int, Int)
packPrefix roundSize keep prefixIndicies offset n = do
    finalKeep <- U.unsafeFreeze keep
    let remainingIndicies = U.filter (> 0) $ U.zipWith (\a b -> (fromBool a) * b) finalKeep prefixIndicies --woohoo faster using zipwith
    let numCommitted = roundSize - (U.length remainingIndicies)
    let !newPrefix = remainingIndicies U.++ (U.enumFromN offset (min (n-offset) numCommitted))
    let newOffset = offset + (min (n-offset) numCommitted)
    --let !x = traceShow (".......committed", numCommitted, "newPrefix: ", U.length newPrefix, ", newOffset", newOffset) 1 
    --return $ (if (U.length newPrefix > 0) 
    --    then ((newPrefix U.! 0) `seq` (numCommitted, newPrefix, rest)) else  (numCommitted, newPrefix, rest))
    return $! (numCommitted, newPrefix, newOffset)
