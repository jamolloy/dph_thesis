{-#  OPTIONS_GHC -XBangPatterns #-}

module Main (main) where

import qualified Data.Vector.Unboxed as U
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as MV

import UnionFind
import ST
import GHC.ST
import Debug.Trace
import DR
import Timing
import SpeculativeFor
import ArgParser

import System.Environment (getArgs)
import Control.Monad (when, replicateM_, liftM)
import Control.Monad.ST (stToIO)
import System.IO
import Data.List.Split

main = do
	args <- getArgs
	(PBBSargs rounds output input) <- compilerOpts args

	--mapM_ (putStrLn . show) $ getPairs1 (B.dropWhile (/= '\n') contents)

	inputVector <- processInput getPair input
	let !inputSize = (U.maximum $ U.map (uncurry max) inputVector) + 1

	let numLinksToMake = (U.length inputVector) 
	putStrLn $ "doing " ++ (show numLinksToMake) ++ " links on " ++ (show inputSize) ++ " elements..."

	--do n rounds. write time to std out. (pbbs-time)
	replicateM_ (rounds - 1) $ testST numLinksToMake inputVector
	result <- testST numLinksToMake inputVector

	writeOutputToFile output result


testST numLinksToMake input = do
	(result, t) <- time $ return $! speculative_for 20 (newStep :: Algo STStep) input
	putStrLn $ pbbsTime t
	return result


