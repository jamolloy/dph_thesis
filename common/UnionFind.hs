module UnionFind (ST', UF, find, makeParents, link, numComponents) where
--path compression implemented

--implemented using vectors in PBBS, but most implementations use a tree forest. 
--But then we need to keep references to initial elements anyway
--search for an element's index with elemIndex is slow. using a hash table would be much faster.

--make sets creates array of parents for index in original array

--updated to use an unordered hashmap form unordered-collections, to improve access to elements parent index.
	--use the strict version, as we know we will be accessing all the elements.

--we can test this unionfind algorithm against the haskell Data.Graph library.

--TODO: how do we create a large test?

import qualified Data.Vector.Unboxed as V
import qualified Data.Vector.Unboxed.Mutable as MV
import qualified Data.Binary as B
import Data.Vector.Binary
import System.IO
import Debug.Trace
import Control.Monad

import GHC.ST



data UF s = UF (MV.MVector s Int)

type ST' s a = ST s (a s)

makeParents :: Int -> ST' s UF
makeParents v = do
	mv <- MV.replicate (v+1) (-1)
	return (UF mv)

find :: UF s -> Int -> ST s Int
find uf@(UF xs) i	= do
	parent <- MV.read xs i
	if parent  == -1 then return i
	else do
		parent2 <- find uf parent
		updateParent uf i $! parent2

link :: UF s -> Int -> Int-> ST s Int
link = do
	v <- updateParent
	return $! v

updateParent :: UF s -> Int -> Int -> ST s Int
updateParent uf@(UF ind) childIndex parentIndex 
	= MV.write ind childIndex parentIndex >> return parentIndex

numComponents (UF v) = liftM nc (freezeMVector v)
	where
		nc = V.length . (V.filter (\x -> (x == -1)))

freezeMVector :: MV.Unbox a => MV.MVector s a -> ST s (V.Vector a)
freezeMVector v = V.generateM (MV.length v) (MV.read v) --isn't this some awesome haskell!
