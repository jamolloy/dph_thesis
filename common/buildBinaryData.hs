import qualified Data.Binary as B
import qualified Data.ByteString.Lazy as BL
import Data.Vector.Binary
import qualified Data.Vector.Unboxed as V
import System.IO
import Data.List.Split

{-# LANGUAGE BangPatterns #-}


main = do
	let name = "randLocalData"
	fromHandle <- openFile ("test/"++ name ++ ".txt") ReadMode
	let toHandle = "test/" ++ name ++ ".data"
	contents <- hGetContents fromHandle
	let (_ : xs) = lines contents
	doEncoding toHandle xs

doEncoding toHandle xs = B.encodeFile toHandle (maxInput+1, vec)
	where 
		maxInput = V.maximum $ V.map (uncurry max) vec
		vec = parseData xs

--need vector-binary-instances packages to encode vectors
parseData :: [String] -> V.Vector (Int, Int)
parseData xs = V.fromList $ map stringToInput xs 

stringToInput :: String -> (Int, Int)
stringToInput s = (x,y)
	where (x:y:_) = map read (splitOn " " s)