
import qualified Data.Vector as V
import qualified Data.Vector.Mutable as MV
import qualified Data.HashMap.Strict as Map 
import Data.Hashable
import Data.Maybe
import System.IO
import Data.List.Split
import Data.List (nubBy)
import Debug.Trace
import Data.Graph

main = do
	fromHandle <- openFile "test/largeUF.txt" ReadMode
	contents <- hGetContents fromHandle
	let (n : xs) = lines contents
	let inputSize = read n
	let numLinksToMake = 2000000
	putStrLn $ "doing " ++ (show numLinksToMake) ++ " inserts on " ++ (show inputSize) ++ " elements..."
	let g = buildGraph inputSize numLinksToMake xs
	putStrLn $ show $ length $ components g

buildGraph inputSize numLinksToMake xs = buildG (0, inputSize) $ take numLinksToMake $ map stringToInput xs 

stringToInput :: String -> (Int, Int)
stringToInput s = (x,y)
	where (x:y:_) = map read (splitOn " " s)


filterInput :: Eq a => [(a, a)] -> [(a, a)]
filterInput xss = nubBy (\(a1,a2) (b1,b2) -> a1 == b1) xss
