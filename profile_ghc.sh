ghc --make -O2 -rtsopts -prof -caf-all -auto-all $1.hs
time ./$1 +RTS -sstderr -h -p -k1g
hp2ps -c $1.hp
