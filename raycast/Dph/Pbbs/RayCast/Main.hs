module Dph.Pbbs.RayCast.Main(main) where

import Dph.Pbbs.RayCast.RayTriangleIntersect
import Dph.Pbbs.RayCast.KdTree
import System.IO
import Data.List
import Debug.Trace


main = do
	fromHandle <- openFile "triangles100.txt" ReadMode
	contents <- hGetContents fromHandle
	let (_ : n : m : xs) = lines contents
	let triangles = getTriangles (read n) (read m) xs
	let tree = buildTree triangles
	let leafNodes = getLeafNodeLengths tree
	mapM_ (putStrLn.show) triangles
	putStrLn $ "AllTriangles Exist: " ++ (show $ checkTrianglesExist triangles tree)
	--putStrLn $ "getTreeDepth: " ++ (show $ getTreeDepth tree)
	putStrLn $ "leafNodeLengths: " ++ (show $ leafNodes)
	putStrLn $ "checkBoxBoundings: " ++ (show $ checkBoxBoundings triangles tree)
	putStrLn $ "num leaf nodes: " ++ (show $ length leafNodes)

checkTrianglesExist :: Triangles -> KdTree -> Bool
checkTrianglesExist xs tree = and $ map (triangleExists tree) (zip [0..] xs)
	

triangleExists :: KdTree -> (Int, Triangle) -> Bool
triangleExists KdEmpty _ = True
triangleExists (KdLeaf indicies _ _) (i, t) = i `elem` indicies
triangleExists (KdNode branches box cutoff axis) (i, t) 
	= (or $ map (\x -> triangleExists x (i, t)) branches)

--check all triangles are within their nodes bounding boxes

--check all nested bounding boxes are within thier parents box
checkBoxBoundings :: Triangles -> KdTree -> Bool
checkBoxBoundings _ KdEmpty = True
checkBoxBoundings tri (KdLeaf indicies box _) = and $ map (\t -> triangleInBox box (tri !! t)) indicies
checkBoxBoundings tri (KdNode branches@(l:r:[]) box1 _ _) = (and $ map (checkBoxBoundings tri) branches) 
			&& and (map (boxInBox box1)  branches)
				
boxInBox :: BoundingBox -> KdTree -> Bool
boxInBox box1 branch = (x (box branch)) `inside` (x box1) 
						&& (y (box branch)) `inside` (y box1) && (z (box branch)) `inside` (z box1)
					where 
						inside = \a b -> ((rmin a) >= (rmin b) && (rmax a) <= (rmax b))

getTreeDepth :: KdTree -> Int
getTreeDepth KdEmpty = 0
getTreeDepth (KdLeaf _ _ _) = 1
getTreeDepth (KdNode (l:r:[]) _ _ _) = 1 + getTreeDepth l

getLeafNodeLengths :: KdTree -> [Int]
getLeafNodeLengths KdEmpty = []
getLeafNodeLengths (KdLeaf branches _ _) = [length branches]
getLeafNodeLengths (KdNode (l:r:[]) _ _ _) = (getLeafNodeLengths l) ++ (getLeafNodeLengths r)


