
module Dph.Pbbs.RayCast.KdTree where

import Data.Trees.KdTree (Point(coord), Point3d(..))
import Data.Array
import qualified Data.List as L
import Data.List.Split
import qualified Data.Vector as V
import Debug.Trace

type Vector3d = Point3d

type Triangles = [Triangle]

data Ray = Ray Point3d Vector3d

data Triangle = Triangle { a :: Point3d, b :: Point3d, c ::Point3d }
	deriving (Eq, Ord)

instance Show Triangle where
	show x = "{"++ pp(a x) ++", "++pp(b x)++", "++pp(c x)++"}"

data KdTree = KdNode { branches :: [KdTree],
                            box :: BoundingBox,
                            cutOff :: Double,
                            kdAxis :: Int }
            | KdLeaf { triangleIndicies :: [Int],
            				box :: BoundingBox,
            				kdAxis :: Int }
            | KdEmpty

data Range = Range { rmin :: Double, rmax :: Double }
		deriving (Show)
data BoundingBox = BoundingBox { x, y, z :: Range }
		deriving (Show)
data EventType = Start | End
		deriving (Eq, Show)
data Event = Event {coordinate :: Double , triI :: Int, types :: EventType }
		deriving (Eq, Show)
instance Ord Event where
	(<=) a b = (coordinate a) <= (coordinate b)


triangleToBB :: Triangle -> BoundingBox
triangleToBB (Triangle a b c) = BoundingBox rx ry rz
					where 	
						rx = Range (min3 p3x a b c) (max3 p3x a b c)
						ry = Range (min3 p3y a b c) (max3 p3y a b c)
						rz = Range (min3 p3z a b c) (max3 p3z a b c)

rangeBB :: Int -> BoundingBox -> Range
rangeBB 0 b = x b
rangeBB 1 b = y b
rangeBB 2 b = z b

comp3 :: Ord a => (a -> a -> a) -> (Point3d -> a) -> Point3d -> Point3d -> Point3d -> a
comp3 comp op a b c = comp (op a) $ comp (op b) (op c)

min3 = comp3 min
max3 = comp3 max

buildTree :: Triangles -> KdTree
buildTree tri = buildTreeWithDepth 0 tri boxes sortedEvents
		where 
			-- only need two event points per triangle (start, end), not a middle too.
			sortedEvents = map L.sort $ zipWith (++) startEvents endEvents
			startEvents = [[Event (rmin $ rangeBB dim b) i Start | (i, b) <- boxes] | dim <- [0..2] ]
			endEvents = [[Event (rmax $ rangeBB dim b) i End | (i, b) <- boxes] | dim <- [0..2] ]
			boxes = map (\(i, t) -> (i, triangleToBB t)) $ zip indicies tri
			indicies = [0..(length tri)-1]

buildTreeWithDepth :: Int -> Triangles -> [(Int, BoundingBox)] ->[[Event]] -> KdTree
buildTreeWithDepth _ [] _ events = KdEmpty
buildTreeWithDepth axis tri  boxes events 	| (length (head events)) == 0 = KdEmpty
											| (length indicies) < 5 	= KdLeaf (trace ("numTri: " ++ (show indicies)) indicies) box axis
											| otherwise 		= KdNode (l:r:[]) box median axis
						where 
							box = calcBox events
							indicies = L.nub $ map (triI) (concat events)
							parFunc = buildTreeWithDepth ((axis + 1) `mod` 3) tri boxes
							median = trace ("median from: " ++ (show box) ++ ": " ++ show(calcMedian box axis) ) $ calcMedian box axis 
							splitE  :: [([Event], [Event])] =  map (splitEvents boxes (trace (show median) median) axis) events
							(lE, rE) :: ([[Event]], [[Event]]) = unzip splitE
							l = trace ((show median) ++ "\n" ++ (show lE) ++ "\n" ++ (show rE)) $ parFunc lE
							r = parFunc rE
							--need to split events using the boxes for a triangle

--split events left or right if they fall on a certain side of the median
	-- only need boxes range for that dimension
splitEvents :: [(Int, BoundingBox)] -> Double -> Int -> [Event] -> ([Event], [Event])
splitEvents boxes cutoff dim events = (left, right)
			where 
				boxes1 :: [(Int, Range)]	= [(i, rangeBB dim b) | (i, b) <- boxes] 
				--filter events if box min for that event is less than cutoff  
				left 	= [e | e <- events, cutoff > rmin (snd $ boxes1 !! (triI e)) ]
				right   = [e | e <- events, cutoff <= rmax (snd $ boxes1 !! (triI e)) ]
				--left = take ((length events) `div` 2) events
				--right = drop ((length events) `div` 2) events    -- trace ((show cutoff) ++ (show boxes1)) $ 


calcMedian :: BoundingBox -> Int -> Double
calcMedian bb 0 = (rmax (x bb) + rmin (x bb)) / 2
calcMedian bb 1 = (rmax (y bb) + rmin (y bb)) / 2
calcMedian bb 2 = (rmax (z bb) + rmin (z bb)) / 2

--			triangles, indicies, dimension, bounding box
calcBox :: [[Event]] -> BoundingBox
calcBox events = trace (show (res)) $ BoundingBox x y z
			where 
				res@(x:y:z:_) = [ Range (minimum ev1) (maximum ev1) | ev1 <- [ map coordinate e | e <- events ]]

						--dimOfTri = map ()


getTriangles :: Int -> Int -> [String] -> Triangles
getTriangles n m xs = map (stringToPointsToTriangle pointsV) triangleStrings
			where 
				(pointStrings, triangleStrings) =  splitAt n xs
				pointsV = V.fromList $ map stringToPoint pointStrings
				--get 3 indexes, map indexes onto pointStringsVector, create triangle from point

stringToPointsToTriangle :: V.Vector Point3d -> String -> Triangle
stringToPointsToTriangle v s = Triangle (v V.! x) (v V.! y) (v V.! z)
			where 
				(x:y:z:[]) = map read (splitOn " " s)

stringToPoint :: String -> Point3d
stringToPoint s = Point3d x y z 
				where (x:y:z:[]) = map read (splitOn " " s)


pp :: Point3d -> String
pp (Point3d x y z) = "["++show x++", "++show y++", "++show z++"]"

scaleVector :: Vector3d -> Vector3d -> Double -> Vector3d
scaleVector (Point3d x y z) (Point3d x1 y1 z1) v = Point3d (x+v*x1) (y+v*y1) (z+v*z1)

filterBox :: Ray -> BoundingBox -> (Int, Double) -> Bool
filterBox (Ray o d) box (_,t) = t > 0.0 && inBox box (scaleVector o d t)

smallestIndex :: Ord a => [(Int, a)] -> (Int, a)
smallestIndex (x:xs) = foldr minS x xs
	where minS x@(x1,x2) y@(y1,y2) = if (x2 <= y2) then x else y

inBox :: BoundingBox -> Point3d -> Bool
inBox (BoundingBox bx by bz) (Point3d x y z) = xok && yok && zok
		where 
			xok = (x >= rmin bx - epsilon) && (x <= rmax bx + epsilon)
			yok = (y >= rmin by - epsilon) && (y <= rmax by + epsilon)
			zok = (z >= rmin bz - epsilon) && (z <= rmax bz + epsilon)
			epsilon = 0.0000001

triangleInBox :: BoundingBox -> Triangle -> Bool
triangleInBox bb (Triangle a b c) = inBoxB a || inBoxB b || inBoxB c
	where inBoxB = inBox bb