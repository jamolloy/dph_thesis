--based on rayTriangleIntersect.h from pbbs rayCast impl.

module Dph.Pbbs.RayCast.RayTriangleIntersect where

import Data.Trees.KdTree (Point, Point3d(..))
import Data.Array
import Data.List.Split
import Dph.Pbbs.RayCast.KdTree


data RayPath = LR | RL | L | R



--read in triangles
--first line is number of points, second is number of triangles

--need to take into account no intersections.
--do triangle intersect
--filter triangles based on indicies, calculate ray triangle intersect, and take the min
findRay :: Ray -> KdTree -> Triangles -> Int
findRay ray@(Ray o d) (KdLeaf indicies box axis) tri 
	= if (length a == 0) then -1 else fst $ smallestIndex a
		where a = filter (filterBox ray box) $ zip ([0..]) $ map (rayTriangleIntersect ray) tri  

findRay ray@(Ray o d) n@(KdNode (x:xs:[]) box cutOff axis) tri
		= case calcRayPath ray n of 
			L -> v1
			R -> v2 --TODO: check that these are lazy
			LR -> if (v1 >= 0) then v1 else v2
			RL -> if (v2 >= 0) then v2 else v1
		where v1 = findRay ray (x) tri
		      v2 = findRay ray (xs) tri

calcRayPath :: Ray -> KdTree -> RayPath
calcRayPath _ _ = undefined



rayTriangleIntersect :: Ray -> Triangle -> Double
rayTriangleIntersect (Ray o d) (Triangle a b c) | det > -epsilon && det < epsilon = 0
							 					| u < 0.0 || u > 1.0 = 0
							 					| v < 0.0 || u+v > 1.0 = 0
							 					| otherwise = (e2 `dot` qvec) * invDet
							 					where 
							 						e1 = b `subVectors` a
							 						e2 = c `subVectors` a
							 						pvec = d `cross` e2
							 						det = e1 `dot` pvec
							 						invDet = (1.0/det)
							 						tvec = o `subVectors` a
							 						u = tvec `dot` pvec * invDet
							 						qvec = tvec `cross` e1
							 						v = d `dot` qvec * invDet
							 						epsilon = 0.00000000001



dot :: Vector3d -> Vector3d -> Double
dot (Point3d x y z) (Point3d xx yy zz) = x*xx + y*yy + z*zz

cross :: Vector3d -> Vector3d -> Vector3d
cross (Point3d a1 a2 a3) (Point3d b1 b2 b3) = Point3d (a2*b3-a3*b2) (a3*b1-a1*b3) (a1*b2-a2*b1)

subVectors :: Vector3d -> Vector3d -> Vector3d
subVectors (Point3d b1 b2 b3) (Point3d a1 a2 a3) = Point3d (b1-a1) (b2-a2) ( b3-a3)